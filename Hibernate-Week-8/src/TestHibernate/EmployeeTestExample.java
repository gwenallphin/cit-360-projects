package TestHibernate;

import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Hibernate;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class EmployeeTestExample {

    public static void main(String[] args) {
        TestDAO test = TestDAO.getInstance();

        TestDAO.addEmployee("Christian", "Allphin", 15000);
        TestDAO.addEmployee("Liz", "Allphin", 20000);
        TestDAO.addEmployee("Emily", "Kellett", 25000);


        List<Employee> emp = test.getEmployees();
        for (Employee i : emp) {
            System.out.println(i);
        }


        System.out.println(test.getEmployee(13));
    }
}
