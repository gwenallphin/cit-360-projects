package TestHibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.*;

public class TestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO() {
        factory = UtilitiesHibernate.getSessionFactory();
    }
    public static TestDAO getInstance() {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }
        return single_instance;
    }

    public static void addEmployee(String firstName, String lastName, int salary) {
        Session session = UtilitiesHibernate.getSession();
        try {
            session.getTransaction().begin();
            Employee employee = new Employee();
            employee.setFirstName(firstName);
            employee.setLastName(lastName);
            employee.setSalary(salary);
            session.save(employee);
            session.getTransaction().commit();
            System.out.println("Employee " + firstName + " has been added");
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

    }



    public List<Employee> getEmployees() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from TestHibernate.Employee";
            List<Employee> emp = (List<Employee>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return emp;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Employee getEmployee(int idEmployee) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from TestHibernate.Employee where idEmployee=" + Integer.toString(idEmployee);
            Employee semp = (Employee)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return semp;
        } catch (Exception e){
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
