package TestHibernate;
import javax.persistence.*;

@Entity
@Table(name = "pets")
public class pets {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpets")
    private int idpets;

    @Column(name = "petName")
    private String petName;

    @Column(name = "petSpecies")
    private String petSpecies;

    @Column(name = "petAge")
    private int petAge;

    public int getIdpets() {
        return idpets;
    }

    public void setIdpets(int idpets) {
        this.idpets = idpets;
    }

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPetSpecies() {
        return petSpecies;
    }

    public void setPetSpecies(String petSpecies) {
        this.petSpecies = petSpecies;
    }

    public int getPetAge() {
        return petAge;
    }

    public void setPetAge(int petAge) {
        this.petAge = petAge;
    }

    @Override
    public String toString() {
        return "pets{" +
                "idpets=" + idpets + ", petName='" + petName + '\'' + ", petSpecies='" + petSpecies + '\'' +
                ", petAge=" + petAge + '}';
    }
}
