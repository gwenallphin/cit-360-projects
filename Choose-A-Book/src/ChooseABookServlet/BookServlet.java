package ChooseABookServlet;

import jdk.jshell.spi.ExecutionControl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

@WebServlet(name = "BookServlet", urlPatterns ={"/BookServlet"})
public class BookServlet extends HttpServlet {
    private BookDAO bookDao;

    public void init() {
        bookDao = new BookDAO();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter print = response.getWriter();
        response.setContentType("text/html");
        String myGenre = request.getParameter("genre");

        switch (myGenre) {
            case "mystery":
                print.println("you chose mystery");

            case "romance":
                print.println("you chose a romance");
                break;
            case "horror":
                print.println("you chose horror");
                break;
            case "historical":
                print.println("you chose historical");
                break;

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter print = response.getWriter();
        print.println("this resource is not available directly.");
    }
}
