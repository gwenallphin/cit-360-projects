package ChooseABookServlet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.awt.print.Book;
import java.util.List;

public class BookDAO {
    SessionFactory factory = null;
    Session session = null;

    private static BookDAO single_instance = null;

    protected BookDAO() { factory = UtilitiesHibernate.getSessionFactory(); }

    public static BookDAO getInstance() {
        if (single_instance == null) {
            single_instance = new BookDAO();
        }
        return single_instance;
    }

    public static void addBook(String link, String title, String authors, String description, int pageCount, String genre) {
        Session session = UtilitiesHibernate.getSession();
        try {
            session.getTransaction().begin();
            Books book = new Books();
            book.setLink(link);
            book.setTitle(title);
            book.setAuthors(authors);
            book.setDescription(description);
            book.setPageCount(pageCount);
            book.setGenre(genre);
            session.save(book);
            session.getTransaction().commit();
            } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            } finally {
            session.close();
        }
    }

    public List<Books> getBooks(){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from choose-a-book.Books";
            List<Books> books = (List<Books>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return books;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Books getBook(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Choose-A-Book.Books where id=" + Integer.toString(id);
            Books book = (Books)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return book;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }


}
