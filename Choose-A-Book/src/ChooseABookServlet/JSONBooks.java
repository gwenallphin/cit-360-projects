package ChooseABookServlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONBooks {
    public static String bookToJSON(Books book) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(book);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Books JSONToBook(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Books book = null;

        try {
            book = mapper.readValue(s, Books.class);
        } catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return book;

    }
}
