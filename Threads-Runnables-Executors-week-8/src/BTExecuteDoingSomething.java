
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BTExecuteDoingSomething {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        BTDoingSomething ds1 = new BTDoingSomething("Bob", 25, 1000);
        BTDoingSomething ds2 = new BTDoingSomething("Sally", 10, 500);
        BTDoingSomething ds3 = new BTDoingSomething("Billy", 5, 250);
        BTDoingSomething ds4 = new BTDoingSomething("Anna", 2, 100);
        BTDoingSomething ds5 = new BTDoingSomething("Pat", 1, 50);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}

