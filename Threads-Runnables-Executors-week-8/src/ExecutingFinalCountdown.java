import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutingFinalCountdown {

    public static void main(String[] args) {

        ExecutorService countdown = Executors.newFixedThreadPool(3);

        FinalCountdown person1 = new FinalCountdown("Chris", 25, 30);
        FinalCountdown person2 = new FinalCountdown("Emily", 25, 40);
        FinalCountdown person3 = new FinalCountdown("Liz", 22, 50);
        FinalCountdown person4 = new FinalCountdown("Sam", 22, 60);
        FinalCountdown person5 = new FinalCountdown("Sydney", 23, 70);
        FinalCountdown person6 = new FinalCountdown("Jeff", 57, 80);
        FinalCountdown person7 = new FinalCountdown("Jan", 60, 90);
        FinalCountdown person8 = new FinalCountdown("Scott", 69, 100);
        FinalCountdown person9 = new FinalCountdown("Gwen", 54, 110);

        countdown.execute(person1);
        countdown.execute(person2);
        countdown.execute(person3);
        countdown.execute(person4);
        countdown.execute(person5);
        countdown.execute(person6);
        countdown.execute(person7);
        countdown.execute(person8);
        countdown.execute(person9);

        countdown.shutdown();
    }
}
