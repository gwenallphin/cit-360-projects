import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutingSummerPopping {

    public static void main(String[] args) {
        ExecutorService popcornService = Executors.newFixedThreadPool(4);

        SummerPopping popcornPopping1 = new SummerPopping("Apple", 69, 50);
        SummerPopping popcornPopping2 = new SummerPopping("Peach", 54, 100);
        SummerPopping popcornPopping3 = new SummerPopping("Walnut", 25, 150);
        SummerPopping popcornPopping4 = new SummerPopping("Pine", 22, 200);
        SummerPopping popcornPopping5 = new SummerPopping("Birch", 56, 250);
        SummerPopping popcornPopping6 = new SummerPopping("Cherry", 6, 300);
        SummerPopping popcornPopping7 = new SummerPopping("Ash", 98, 350);
        SummerPopping popcornPopping8 = new SummerPopping("Choke Cherry", 2, 400);
        SummerPopping popcornPopping9 = new SummerPopping("Maple", 34, 450);
        SummerPopping popcornPopping10 = new SummerPopping("Lilac", 16, 500);

        popcornService.execute(popcornPopping1);
        popcornService.execute(popcornPopping2);
        popcornService.execute(popcornPopping3);
        popcornService.execute(popcornPopping4);
        popcornService.execute(popcornPopping5);
        popcornService.execute(popcornPopping6);
        popcornService.execute(popcornPopping7);
        popcornService.execute(popcornPopping8);
        popcornService.execute(popcornPopping9);
        popcornService.execute(popcornPopping10);

        popcornService.shutdown();

    }
}
