import java.util.Random;

public class SummerPopping implements Runnable {

    private String pop;
    private int tree;
    private int apricot;
    private int popcorn;

    public SummerPopping(String pop, int tree, int apricot) {

        this.pop = pop;
        this.tree = tree;
        this.apricot = apricot;

        Random random = new Random();
        this.popcorn = random.nextInt(500);
    }

    @Override
    public void run() {
        System.out.println("We start the day with a " + pop + " on the " + tree + " tree, with a bit of a break " + apricot + " for a very very long nap " + popcorn + "\n");

        for (int i = 1; i < popcorn; i++) {
            if (i % tree == 0) {
                System.out.println("The trees are popping: " + pop + " tree has: " + i + " pops!");
                try {
                    Thread.sleep(apricot);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }

        System.out.println("\n" + pop + " is finished popping. \n");
    }
}
