import java.util.Random;

public class FinalCountdown implements Runnable {

    private int randomNumber;
    private String person;
    private int age;
    private int napTime;

    public FinalCountdown(String person, int age, int napTime) {
        this.person = person;
        this.age = age;
        this.napTime = napTime;

        Random random = new Random();
        this.randomNumber = random.nextInt(50);
    }

    @Override
    public void run() {
        System.out.println(person + " is " + age + " years old. We'll count up from: " + randomNumber + "\n\n");

        for (int i = 1; i < randomNumber; i++) {
            System.out.println(person + "'s count is: " + i + " on the way to " + randomNumber);
            try {
                Thread.sleep(napTime);
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
        System.out.println("\n" + person + " is finished with the count up!\n\n");
    }
}
