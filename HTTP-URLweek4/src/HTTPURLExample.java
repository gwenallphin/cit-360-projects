import java.net.*;
import java.io.*;

public class HTTPURLExample {
    public static void main(String[] args ) throws Exception {
        URL twitter = new URL("https://twitter.com/");
        URLConnection yt = twitter.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(yt.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) !=null)
            System.out.println(inputLine);
        in.close();
    }

}
