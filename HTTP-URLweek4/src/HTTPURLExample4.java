import java.net.*;

public class HTTPURLExample4 {
    public static void main(String[] args) throws MalformedURLException{
        // create an url with a string representation

        URL firstURL = new URL("https://byui.instructure.com/courses/95920/assignments/3681543");

        // create an URL with a protocol, hostname, and path

        URL secondURL = new URL("http", "www.nieniedialogues.com", "/2020/05/commemoration.html");

        URL thirdURL = new URL("https://www.deseret.com/faith/2020/3/30/21200407/april-general-conference-2020-mormon-church-lds-salt-lake-city-talk-summaries-photo-galleries");

        // print the URL
        System.out.println(firstURL.toString());
        System.out.println(secondURL.toString());
        System.out.println();
        System.out.println("Different components of the third URL:");

        // Retrieve the protocol for the URL
        System.out.println("Protocol: " + thirdURL.getProtocol());

        // Retrieve the hostname of the URL
        System.out.println("HostName: " + thirdURL.getHost());

        // Retrieve the default port of the URL
        System.out.println("Default Port: " + thirdURL.getDefaultPort());

        // Retrieve the path of the URL
        System.out.println("Path: " + thirdURL.getPath());

        // Retrieve the file name of the URL
        System.out.println("File Name" + thirdURL.getFile());

    }
}
