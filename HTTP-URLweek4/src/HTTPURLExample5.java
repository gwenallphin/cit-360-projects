import java.io.*;
import java.net.*;

public class HTTPURLExample5 {

    public static void main(String[] args) throws IOException {

        URL linkOne = new URL("https://latterdaysaintmag.com/cartoon-earth-life-orientation/");

        URL linkTwo = new URL("https","www.ldsliving.com", "/Tim-Ballard-Explains-How-a-Copy-of-the-Book-of-Mormon-Signed-by-Joseph-Smith-Ended-Up-in-the-John-Adams-House-Library/s/92854");

        // print the URL's to the console

        System.out.println("The entire URL: ");
        System.out.println(linkOne);
        System.out.println(linkTwo);

        // components of the links
        System.out.println();
        System.out.println("Different parts of the URL: ");

        // Retrieve the protocol for the URL
        System.out.println("Protocol for 1st link: " + linkOne.getProtocol());

        // Retrieve the hostname of the URL
        System.out.println("HostName for 2nd link: " + linkTwo.getHost());

        // Retrieve the default port of the URL
        System.out.println("Default Port 1st link: " + linkOne.getDefaultPort());
        System.out.println("Default Port 2nd link: " + linkTwo.getDefaultPort());

        // Retrieve the path of the URL
        System.out.println("Path for 1st link: " + linkTwo.getPath());

        // Retrieve the file name of the URL
        System.out.println("File Name of 1st link: " + linkOne.getFile());

        // Retrieve User Information
        System.out.println("Authority of 2nd link:  " + linkTwo.getAuthority());

        // Retrieve content
        System.out.println("Content from 1st link: " + linkOne.getContent());


        System.out.println("File from 2nd link: " + linkTwo.getFile());


    }
}
