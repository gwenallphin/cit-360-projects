public class Pet {
    private String name;
    private String animal;
    private int age;
    private String likes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public long getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String toString() {
        return "Name: " + name + "; Animal :" + animal + "; Age: " + age + "; Likes: " + likes;
    }

}



