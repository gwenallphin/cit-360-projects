import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONPetExamples {
    public static String petToJSON(Pet pet) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(pet);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Pet JSONToPet(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Pet pet = null;

        try {
            pet = mapper.readValue(s, Pet.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return pet;
    }

    public static void main(String[] args) {

        Pet myPet = new Pet();
        myPet.setName("Minda");
        myPet.setAnimal("Dog");
        myPet.setAge(2);
        myPet.setLikes("Chewing");

        String json = JSONPetExamples.petToJSON(myPet);
        System.out.println(json);

        Pet pet2 = JSONPetExamples.JSONToPet(json);
        System.out.println(pet2);

        Pet myPet2 = new Pet();
        myPet2.setName("IzzyBella");
        myPet2.setAnimal("Dog");
        myPet2.setAge(5);
        myPet2.setLikes("Chasing Rabbits");

        String json2 = JSONPetExamples.petToJSON(myPet2);
        System.out.println(json2);

        Pet pet3 = JSONPetExamples.JSONToPet(json2);
        System.out.println(pet3);
    }


}
