import org.junit.Test;
import static org.junit.Assert.*;

public class AssertMethods {
    @Test
    public void testAssertions() {
        String first = new String("Gwendolyn");
        String second = new String("Scott");
        String third = null;
        String fourth = "Gwendolyn";


        int number1 = 123;
        int number2 = 456;

        String[] firstArray = {"Scott", "Gwen", "Chris", "Liz"};
        String[] secondArray = {"Scott", "Gwen", "Chris", "Liz"};

        // Check that two objects are equal
        assertEquals(first, fourth);

        // Check that a condition is true
        assertTrue(number1 < number2);

        // Check that a condition is false
        assertFalse(number1 > number2);

        // Check that an object isn't null
        assertNotNull(second);

        // Check that an object is null
        assertNull(third);

        // check is two object references pint to the same object
        assertSame(first, first);

        // Check if two object references not point to the same object
        assertNotSame(first, second);

        // check whether two arrays are equal to each other
        assertArrayEquals(firstArray, secondArray);

    }
}
