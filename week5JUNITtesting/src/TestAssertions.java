import org.junit.Test;
import static org.junit.Assert.*;

public class TestAssertions {

    @Test
    public void testAssertions() {
        // test data
        String string1 = new String("abc");
        String string2 = new String("abc");
        String string3 = null;
        String string4 = "abc";
        String string5 = "abc";

        int value1 = 5;
        int value2 = 6;

        String[] expectedArray = {"one","two","three"};
        String[] resultArray = {"one","two","three"};

        // Check that two objects are equal
        assertEquals(string1,string2);

        // Check that a condition is true
        assertTrue(value1 < value2);

        // Check that a condition is false
        assertFalse(value1 > value2);

        // Check that an object isn't null
        assertNotNull(string1);

        // Check that an object is null
        assertNull(string3);

        // check is two object references pint to the same object
        assertSame(string4,string5);

        // Check if two object references not point to the same object
        assertNotSame(string1,string3);

        // check whether two arrays are equal to each other
        assertArrayEquals(expectedArray,resultArray);
    }
}
