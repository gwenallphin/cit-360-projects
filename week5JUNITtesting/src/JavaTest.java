import junit.framework.*;

public class JavaTest extends TestCase {
    protected int value1, value2;

    // Assign the values
    protected void setUp()  {
        value1 = 3;
        value2 = 3;
    }

    // Test method to add two values
    public void testAdd() {
        double result = value1 + value2;
        assertTrue(result == 6);
    }
}
