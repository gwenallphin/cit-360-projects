import org.junit.*;
import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @BeforeClass
    public static void setUpClass() {
        // code executed before all test methods
    }

    @Before
    public void setUp(){
        // code executed before each test method
    }

    @Test
    public void testAddition() {
        // test method
        Calculator calculator = new Calculator();
        int a = 1234;
        int b = 5678;

        int actual = calculator.addition(a, b);

        int expected = 6912;

        assertEquals("The sum is not correct",expected, actual);
        System.out.println("Addition tested");
    }

    @Test
    public void testSubtraction(){
        // test subtraction
        Calculator calculator = new Calculator();
        int a = 1234;
        int b = 5678;

        int actual = calculator.subtraction(b, a);

        int expected = 4444;

        assertEquals("The sum is incorrect", expected,actual);
        System.out.println("Subtraction Tested");
    }

    @Test
    public void testMultiplication() {
        // test multiplication
        Calculator calculator = new Calculator();

        int a = 1234;
        int b = 5678;

        int actual = calculator.multiplication(a, b);

        int expected = 7006652;

        assertEquals("The sum is incorrect", actual,expected);
        System.out.println("Multiplication Tested");
    }

    @Test
    public void testDivision() {
        // test division
        Calculator calculator = new Calculator();
        int a = 1234;
        int b = 5678;

        int actual = calculator.division(b, a);

        int expected = 4;

        assertEquals("The Division is incorrect", actual,expected);
        System.out.println("Division Tested");

    }

    @After
    public void tearDown(){
        // code executed after each test method
    }

    @AfterClass
    public static void tearDownClass() {
        // code executed after all test methods
    }
}
