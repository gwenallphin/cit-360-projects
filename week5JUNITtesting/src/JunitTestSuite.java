import junit.framework.*;


public class JunitTestSuite {
    public static void main(String[] a) {
        // add the tests in the suite
        TestSuite suite = new TestSuite(TestJunitA.class, TestJunitB.class, TestJunitC.class);
        TestResult result = new TestResult();
        suite.run(result);
        System.out.println("Number of test cases = " + result.runCount());
    }
}
