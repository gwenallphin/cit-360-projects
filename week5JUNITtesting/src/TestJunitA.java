import org.junit.Test;
import static org.junit.Assert.*;

public class TestJunitA {
    @Test
    public void testAdd() {
        // Test Data
        int num = 5;
        String temp = null;
        String str = "Junit is working fine";

        // check for equality
        assertEquals("Junit is working fine", str);

        // check for False condition
        assertFalse(num > 6);

        // check for not null
        //assertNotNull(temp);
    }
}
