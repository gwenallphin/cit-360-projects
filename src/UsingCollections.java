import java.util.*;

public class UsingCollections {

    public static void main(String args[]) {

        try {
            // Linked list Demo
            System.out.println();
            // Create a Linked List
            LinkedList lList = new LinkedList();

            // Add items to the Linked List
            lList.add("Minda");
            lList.add("Izzy");
            lList.add("Spaz");
            lList.add("Pixy");
            lList.add("Boomie");
            lList.add("Akoda");
            lList.add("Cinder");
            lList.addFirst("Oliver");
            lList.addLast("Dad's Pet");

            System.out.println("All of our pets: " + lList);

            // Remove pets from the list
            lList.removeFirst();
            lList.remove("Spaz");
            lList.removeLast();

            System.out.println("All of our current pets: " + lList);

            // Get a pet name and set it to a different name
            Object pet = lList.get(1);
            lList.set(1, (String) pet + "Bella");
            System.out.println("Pets after name change: " + lList);

            System.out.println();

        /*
         Sorted Set Demo
         Create a sorted set
        */

            SortedSet siblings = new TreeSet();

            // Add elements to the set
            siblings.add("Linda");
            siblings.add("Debbie");
            siblings.add("Alan");
            siblings.add("Jeff");
            siblings.add("Gwen");

            // iterating over the elements in the set
            Iterator it = siblings.iterator();

            System.out.println("Siblings in Alphabetical order");
            while (it.hasNext()) {
                // get the sibling
                Object element = it.next();
                System.out.println(element.toString());
            }
            System.out.println();

        /*
        Sorted Map Demo

        Create a Sorted Map
        */

            System.out.println("Show list of names and amounts");
            HashMap mapTest = new HashMap();

            // Insert elements into the map
            mapTest.put("Bob", 5223.26);
            mapTest.put("Brenda", 235.45);
            mapTest.put("Ken", -99.21);
            mapTest.put("Jeanette", 44551.55);
            mapTest.put("Renee", 6545.74);

            Set peeps = mapTest.entrySet();

            // initialize an iterator
            Iterator j = peeps.iterator();

            // Display elements
            while (j.hasNext()) {
                Map.Entry mapEntry = (Map.Entry) j.next();
                System.out.print(mapEntry.getKey() + ": $");
                System.out.println(mapEntry.getValue());
            }
            System.out.println();

            // Change the amount of one of the numbers
            double amount = ((Double) mapTest.get("Jeanette")).doubleValue();
            mapTest.put("Jeanette", (amount - 11543.54));
            System.out.println("Jeanette's new amount: " + mapTest.get("Jeanette"));
            System.out.println();


            // Priority Queue Demo

            // Create a priority queue
            PriorityQueue cars = new PriorityQueue();

            // Add cars to the queue
            cars.add("Ford Pinto");
            cars.add("Ford Escort");
            cars.add("Suzuki Swift");
            cars.add("Ford Focus");
            cars.add("Nissan Altima");
            cars.add("Ford Windstar");
            cars.add("Hyundai SantaFe");

            // Print the first car
            System.out.println("One of the first cars I owned was: " + cars.peek());

            // Print all of the cars
            System.out.println();
            System.out.println("All of the cars that I owned: ");
            Iterator numCar = cars.iterator();
            while (numCar.hasNext())
                System.out.println(numCar.next());

            // Check if a car is present
            boolean carCheck = cars.contains("Ford Windstar");
            System.out.println();
            System.out.println("Do I own a Ford Windstar? " + carCheck);

            // remove Ford Windstar from list
            System.out.println();
            cars.remove("Ford Windstar");
            System.out.println("Actual cars owned:");
            Iterator numCar2 = cars.iterator();
            while (numCar2.hasNext())
                System.out.println(numCar2.next());

            // Check if a car is present
            boolean carCheck2 = cars.contains("Ford Windstar");
            System.out.println();
            System.out.println("Do I own a Ford Windstar? " + carCheck2);

            // Get items from the queue using an array
            Object[] array = cars.toArray();
            System.out.println();
            System.out.println("Cars in the array:");
            for (int k = 0; k < array.length; k++)
                System.out.println("Car: " + array[k].toString());


            // Tree Set Demo

            // Create a tree set
            TreeSet pokemon = new TreeSet();
            pokemon.add("Chespin");
            pokemon.add("Frogadier");
            pokemon.add("Fletchinder");
            pokemon.add("Gourgeist");

            // list pokemon
            System.out.println();
            System.out.println("Pokemon List");
            Iterator poke1 = pokemon.iterator();
            while (poke1.hasNext()) {
                System.out.println(poke1.next());
            }
            // list pokemon in reverse order
            System.out.println();
            System.out.println("Listing Pokemon in reverse order");
            Iterator poke2 = pokemon.descendingIterator();
            while (poke2.hasNext()) {
                System.out.println(poke2.next());
            }

        } catch (Exception e){
            System.out.println("There was an error.");
        }

        }
}
