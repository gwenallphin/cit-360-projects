package servlettest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletExperiment", urlPatterns = {"/ServletExperiment"})
public class ServletExperiment extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        String myPokemon = request.getParameter("pokemon");
        switch (myPokemon) {
            case "grookey":
                pw.println("You chose: Grookey, who is a grass pokemon");
                break;
            case "scorbunny":
                pw.println("You chose: Scorbunny, who is a fire pokemon");
                break;
            case "sobble":
                pw.println("You chose: Sobble, who is a water pokemon");
                break;
            case "bulbasaur":
                pw.println("You chose: Bulbasaur, who is a grass pokemon");
                break;
            case "charmander":
                pw.println("You chose: Charmander, who is a fire pokemon");
                break;
            case "squirtle":
                pw.println("You chose: Squirtle, who is a water pokemon");
                break;


        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.println("This resource is not available directly");
    }
}
